# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                               #
#    lunasm                                                                     #
#    Makefile - used by make utility to compile program                         #
#    Copyright (C) 2015 Arman Hajishafieha                                      #
#                                                                               #
#    This file is part of lunasm.                                               #
#                                                                               #
#    lunasm is free software: you can redistribute it and/or modify             #
#    it under the terms of the GNU General Public License as published by       #
#    the Free Software Foundation, either version 3 of the License, or          #
#    (at your option) any later version.                                        #
#                                                                               #
#    lunasm is distributed in the hope that it will be useful,                  #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#    GNU General Public License for more details.                               #
#                                                                               #
#    You should have received a copy of the GNU General Public License          #
#    along with lunasm.  If not, see <http://www.gnu.org/licenses/>.            #
#                                                                               #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
VERSION=0
SUBVERSION=1
PATCH=14

CC=gcc
LD=gcc
CFLAGS=-g
SRCDIR=src
BINFILE=lunasm
INSTALLDIR=/usr/local/bin

all: release


release: src/main.c
	$(CC) $(CFLAGS) -DPROGRAM_VERSION=$(VERSION) -DPROGRAM_SUBVERSION=$(SUBVERSION) -DPROGRAM_PATCH=$(PATCH) -c $(SRCDIR)/main.c -o main.o
	$(LD) main.o -o $(BINFILE)


install:
	install $(BINFILE) $(INSTALLDIR)

clean:
	rm main.o $(BINFILE) 2>/dev/null
