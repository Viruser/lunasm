/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                               *
 *    lunasm                                                                     *
 *    constants.h - Instructions and constants used to maintain readability      *
 *    Copyright (C) 2015 Arman Hajishafieha                                      *
 *                                                                               *
 *    This file is part of lunasm.                                               *
 *                                                                               *
 *    lunasm is free software: you can redistribute it and/or modify             *
 *    it under the terms of the GNU General Public License as published by       *
 *    the Free Software Foundation, either version 3 of the License, or          *
 *    (at your option) any later version.                                        *
 *                                                                               *
 *    lunasm is distributed in the hope that it will be useful,                  *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *    GNU General Public License for more details.                               *
 *                                                                               *
 *    You should have received a copy of the GNU General Public License          *
 *    along with lunasm.  If not, see <http://www.gnu.org/licenses/>.            *
 *                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define INSTRUCTION_NOP		0x00
#define INSTRUCTION_MOV 	0x04
#define INSTRUCTION_JMP 	0x08
#define INSTRUCTION_JZ 		0x0C
#define INSTRUCTION_JNZ 	0x10
#define INSTRUCTION_JO 		0x14
#define INSTRUCTION_JNO 	0x18
#define INSTRUCTION_JC 		0x1C
#define INSTRUCTION_JNC 	0x20
#define INSTRUCTION_JS 		0x24
#define INSTRUCTION_JNS 	0x28
#define INSTRUCTION_JIR 	0x2C
#define INSTRUCTION_JNIR 	0x30
#define INSTRUCTION_JOR 	0x34
#define INSTRUCTION_JNOR 	0x38
#define INSTRUCTION_CALL 	0x3C
#define INSTRUCTION_RETN 	0x3F
#define INSTRUCTION_ADD 	0x40
#define INSTRUCTION_SUB 	0x44
#define INSTRUCTION_AND 	0x48
#define INSTRUCTION_OR 		0x4C
#define INSTRUCTION_XOR 	0x50
#define INSTRUCTION_MUL 	0x54
#define INSTRUCTION_DIV 	0x58
#define INSTRUCTION_SET 	0x60
#define INSTRUCTION_USET 	0x64
#define INSTRUCTION_INC 	0x68
#define INSTRUCTION_SHL 	0x6C
#define INSTRUCTION_SHR 	0x70
#define INSTRUCTION_INPUT 	0x80
#define INSTRUCTION_OUTPUT 	0x84

#define REGISTER_GR1 		0x02
#define REGISTER_GR2 		0x03
#define REGISTER_GR3 		0x04
#define REGISTER_GR4 		0x05

#define FLAGS_ZF			0x00
#define FLAGS_SF			0x10
#define FLAGS_OF			0x20
#define FLAGS_CF			0x30
#define FLAGS_IEN			0x40
#define FLAGS_CEF			0x50
#define FLAGS_IRF			0x60
#define FLAGS_ORF			0x70