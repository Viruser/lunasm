/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                               *
 *    lunasm                                                                     *
 *    main.c - Main program block                                                *
 *    Copyright (C) 2015 Arman Hajishafieha                                      *
 *                                                                               *
 *    This file is part of lunasm.                                               *
 *                                                                               *
 *    lunasm is free software: you can redistribute it and/or modify             *
 *    it under the terms of the GNU General Public License as published by       *
 *    the Free Software Foundation, either version 3 of the License, or          *
 *    (at your option) any later version.                                        *
 *                                                                               *
 *    lunasm is distributed in the hope that it will be useful,                  *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *    GNU General Public License for more details.                               *
 *                                                                               *
 *    You should have received a copy of the GNU General Public License          *
 *    along with lunasm.  If not, see <http://www.gnu.org/licenses/>.            *
 *                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "constants.h"

#define synerror { fprintf(stderr, "%d: Syntax error.\n", linenumber); \
				   return EXIT_FAILURE; }

void option_parser(int, char**);

int main(int argc, char** argv)
{
	if(argc > 1)
		option_parser(argc, argv);
	char* buffer = (char*) malloc(256);
	char* saveptr, *instruction, *op1, *op2;
	char output[4]; //32 bits
	int linenumber = 0;
	while(fgets(buffer, 256, stdin))
	{
		linenumber++;
		if(buffer[strlen(buffer) - 1] == '\n')
			buffer[strlen(buffer) - 1] = '\0';
		if(strchr(buffer, ' '))
			instruction = strtok_r(buffer, " ", &saveptr);
		else 
			instruction = buffer;
		//
		// instruction decoder
		if(!strcasecmp(instruction, "NOP"))
		{
			output[0] = INSTRUCTION_NOP;
			output[1] = 0;
			fwrite(output, 1, 2, stdout);
		}
		else if(!strcasecmp(instruction, "MOV"))
		{
			output[0] = INSTRUCTION_MOV;
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_MOV) // R <= R
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JMP"))
		{
			output[0] = INSTRUCTION_JMP;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JMP)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JZ"))
		{
			output[0] = INSTRUCTION_JZ;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JZ)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JNZ"))
		{
			output[0] = INSTRUCTION_JNZ;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JNZ)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JO"))
		{
			output[0] = INSTRUCTION_JO;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JO)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JNO"))
		{
			output[0] = INSTRUCTION_JNO;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JNO)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JC"))
		{
			output[0] = INSTRUCTION_JC;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JC)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JNC"))
		{
			output[0] = INSTRUCTION_JNC;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JNC)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JS"))
		{
			output[0] = INSTRUCTION_JS;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JS)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JNS"))
		{
			output[0] = INSTRUCTION_JNS;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JNS)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JIR"))
		{
			output[0] = INSTRUCTION_JIR;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JIR)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JNIR"))
		{
			output[0] = INSTRUCTION_JNIR;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JNIR)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JOR"))
		{
			output[0] = INSTRUCTION_JOR;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JOR)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "JNOR"))
		{
			output[0] = INSTRUCTION_JNOR;
			op1 = strtok_r(NULL, " ", &saveptr);
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1 * 16;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2 * 16;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3 * 16;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4 * 16;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[0] += 1;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else if(strchr(op1, '#'))
			{
				output[0] += 2;
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
			}
			else
			{
				output[0] += 3;
				output[1] = 0;
				output[2] = (char) (strtol(op1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1, NULL, 0) % 256);
			}
			if(output[0] == INSTRUCTION_JNOR)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "ADD"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_ADD;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_ADD)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "SUB"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_SUB;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_SUB)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "AND"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_AND;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_AND)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "OR"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_OR;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_OR)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "XOR"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_XOR;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_XOR)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "MUL"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_MUL;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_MUL)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "DIV"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			op2 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL || op2 == NULL)
				synerror
			output[0] = INSTRUCTION_DIV;
			if(!strcasecmp(op1, "GR1"))
				output[1] = REGISTER_GR1;
			else if(!strcasecmp(op1, "GR2"))
				output[1] = REGISTER_GR2;
			else if(!strcasecmp(op1, "GR3"))
				output[1] = REGISTER_GR3;
			else if(!strcasecmp(op1, "GR4"))
				output[1] = REGISTER_GR4;
			else if(strchr(op1, '[') && strchr(op1, ']'))
			{
				output[1] = 0;
				output[2] = (char) (strtol(op1+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op1+1, NULL, 0) % 256);
				output[0] += 2; //M <= R
			}
			else
				synerror
			output[1] *= 16;
			if(!strcasecmp(op2, "GR1"))
				output[1] += REGISTER_GR1;
			else if(!strcasecmp(op2, "GR2"))
				output[1] += REGISTER_GR2;
			else if(!strcasecmp(op2, "GR3"))
				output[1] += REGISTER_GR3;
			else if(!strcasecmp(op2, "GR4"))
				output[1] += REGISTER_GR4;
			else if(strchr(op2, '[') && strchr(op2, ']'))
			{
				if(output[1] == '0')
					synerror
				output[0] += 1; // R <= M
				output[2] = (char) (strtol(op2+1, NULL, 0) / 256);
				output[3] = (char) (strtol(op2+1, NULL, 0) % 256);
				
			}
			else
			{
				output[2] = (char) (strtol(op2, NULL, 0) / 256);
				output[3] = (char) (strtol(op2, NULL, 0) % 256);
				output[0] += 3; // R <= A
			}
			if(output[1] / 16 == 0) // M <= R
				output[1] *= 16;
			if(output[0] == INSTRUCTION_DIV)
				fwrite(output, 1, 2, stdout);
			else
				fwrite(output, 1, 4, stdout);
		}
		else if(!strcasecmp(instruction, "SET"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL)
				synerror
			output[0] = INSTRUCTION_SET;
			if(!strcasecmp(op1, "ZF"))
				output[1] = FLAGS_ZF;
			else if(!strcasecmp(op1, "SF"))
				output[1] = FLAGS_SF;
			else if(!strcasecmp(op1, "OF"))
				output[1] = FLAGS_OF;
			else if(!strcasecmp(op1, "CF"))
				output[1] = FLAGS_CF;
			else if(!strcasecmp(op1, "IEN"))
				output[1] = FLAGS_IEN;
			else if(!strcasecmp(op1, "CEF"))
				output[1] = FLAGS_CEF;
			else if(!strcasecmp(op1, "IRF"))
				output[1] = FLAGS_IRF;
			else if(!strcasecmp(op1, "ORF"))
				output[1] = FLAGS_ORF;
			else
				synerror
			fwrite(output, 1, 2, stdout);
		}
		else if(!strcasecmp(instruction, "USET"))
		{
			op1 = strtok_r(NULL, " ", &saveptr);
			if(op1 == NULL)
				synerror
			output[0] = INSTRUCTION_USET;
			if(!strcasecmp(op1, "ZF"))
				output[1] = FLAGS_ZF;
			else if(!strcasecmp(op1, "SF"))
				output[1] = FLAGS_SF;
			else if(!strcasecmp(op1, "OF"))
				output[1] = FLAGS_OF;
			else if(!strcasecmp(op1, "CF"))
				output[1] = FLAGS_CF;
			else if(!strcasecmp(op1, "IEN"))
				output[1] = FLAGS_IEN;
			else if(!strcasecmp(op1, "CEF"))
				output[1] = FLAGS_CEF;
			else if(!strcasecmp(op1, "IRF"))
				output[1] = FLAGS_IRF;
			else if(!strcasecmp(op1, "ORF"))
				output[1] = FLAGS_ORF;
			else
				synerror
			fwrite(output, 1, 2, stdout);
		}
	}
    return 0;
}

void option_parser(int argc, char** argv)
{
	if(!strcmp(argv[1], "-r"))
	{
		int offset = 0;
		char buffer[2];
		while(fread(buffer, 1, 2, stdin))
			fprintf(stdout, "%d => X\"%02hhX%02hhX\",\n", offset++, buffer[0], buffer[1]);
		fprintf(stdout, "others => X\"0000\");\n");
		exit(0);
	}
	else if(!strcmp(argv[1], "-v"))
	{
		fprintf(stdout, "%s %d.%d.%d\nCopyright (C) 2015  Arman Hajishafieha\n", argv[0], PROGRAM_VERSION,
					  	PROGRAM_SUBVERSION, PROGRAM_PATCH);
		fprintf(stdout, "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n" \
						"This is free software: you are free to change and redistribute it.\n" \
						"There is NO WARRANTY, to the extent permitted by law.\n");
		exit(0);

	}
}
